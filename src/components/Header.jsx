import React from "react";
import { Container, Grid } from "@mui/material";
import Logo from "../assets/icons/Logo";

const Header = () => {
 return (
  <header className="py-3.5">
   <Container maxWidth="false">
    <Grid container>
     <Grid item lg={12} xs={12} className="flex align-center justify-between">
      <div className="logo">
       <Logo />
      </div>
      <div className="menu">
       <a href="#">Login</a>
      </div>
     </Grid>
    </Grid>
   </Container>
  </header>
 );
};

export default Header;
