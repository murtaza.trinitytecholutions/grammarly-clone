import React from "react";
import { Button, Grid } from "@mui/material";

import Tick from "../../assets/images/tick.png";

const PlanCard = ({ points, heading1, heading2, heading3, buttonText }) => {
  return (
    <Grid item lg={3} xs={12}>
      <div className="plan-table">
        <div className="tab-upper">
          <div className="tab-text">
            <span>{heading1}</span>
            <h2>{heading2}</h2>
            <p>{heading3}</p>
          </div>
          <Button disabled className="plan-btn">
            {buttonText}
          </Button>
        </div>
        <div className="list-icon">
          <ul>
            {points.map((point) => (
              <li key={point}>
                <img src={Tick} alt="" />
                <p>{point}</p>
              </li>
            ))}
          </ul>
        </div>
      </div>
    </Grid>
  );
};

export default PlanCard;
