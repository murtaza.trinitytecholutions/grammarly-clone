import React from "react";

import { Button, Container, Grid } from "@mui/material";

import Tick from "../../assets/images/tick.png";
import PlanCard from "./PlanCard";

const Section2 = () => {
  return (
    <>
      <section className="section-2">
        <Container>
          <Grid container className="justify-center gap-16">
            <Grid item xs={12} className="text-center">
              <h1>Up-Level Your Communication</h1>
              <p>
                Unlock Grammarly Premium’s advanced features and suggestions.
              </p>
            </Grid>
            <PlanCard
              heading1="FOR CASUAL WRITING"
              heading2="Free"
              heading3="Basic writing suggestions."
              buttonText="Current Plan"
              points={["Spelling", "Grammar", "Punctuation", "Conciseness"]}
            />
            <PlanCard
              heading1="FOR WORK OR SCHOOL"
              heading2="Premium"
              heading3="Style, tone, and clarity improvements for writing at work and school."
              buttonText="Upgrade to Grammarly Premium"
              points={[
                "Everything in Free",
                "Clarity-focused sentence rewrites",
                "Tone adjustments",
                "Plagiarism detection",
                "Word choice",
                "Formality level",
                "Fluency",
                "Additional advanced suggestions",
              ]}
            />
          </Grid>
        </Container>
      </section>
    </>
  );
};

export default Section2;
