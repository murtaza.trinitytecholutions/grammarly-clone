import { Container, Grid } from "@mui/material";
import React from "react";

const Section3 = () => {
 return (
  <>
   <section className="section-3">
    <Container>
     <Grid container alignItems={"center"} justifyContent={"space-between"}>
      <Grid item lg={4} xs={12} className="pb-8">
       <h1>Get the Job Done</h1>
       <p>Effective communication is the key to making things happen.</p>
      </Grid>
      <Grid item lg={6} xs={12}>
       <div className="video-g">
        <iframe
         width="100%"
         height="300"
         src="https://www.youtube.com/embed/A8m4USFZWOs"
         title="YouTube video player"
         frameborder="0"
         allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
         allowfullscreen
        ></iframe>
       </div>
      </Grid>
     </Grid>
    </Container>
   </section>
  </>
 );
};

export default Section3;
