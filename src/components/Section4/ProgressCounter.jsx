import * as React from "react";
import CircularProgress from "@mui/material/CircularProgress";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import { Grid } from "@mui/material";

function CircularProgressWithLabel(props) {
  return (
    <Box sx={{ position: "relative", display: "inline-flex" }}>
      <CircularProgress variant="determinate" {...props} />
      <Box
        sx={{
          top: 0,
          left: 0,
          bottom: 0,
          right: 0,
          position: "absolute",
          display: "flex",
          alignItems: "center",
          justifyContent: "center"
        }}
      >
        <Typography variant="caption" component="div" color="text.secondary">
          {`${Math.round(props.value)}%`}
        </Typography>
      </Box>
    </Box>
  );
}

const ProgressCounter = ({ maxProgress }) => {
  const [progress, setProgress] = React.useState(1);

  React.useEffect(() => {
    if (progress >= maxProgress) return;
    const timer = setInterval(() => {
      setProgress((prevProgress) =>
        prevProgress >= maxProgress ? 0 : prevProgress + 5
      );
    }, 100);
    return () => {
      clearInterval(timer);
    };
  }, [progress, maxProgress]);

  return (
    <Grid item lg={3}>
      <CircularProgressWithLabel value={progress} />
      <p>76% of Grammarly users find writing more enjoyable.</p>
    </Grid>
  );
};

export default ProgressCounter;
