import * as React from "react";
import { Grid } from "@mui/material";

import ProgressCounter from "./ProgressCounter";
import useOnScreen from "../../hooks/useOnScreen";

export default function CircularStatic() {
  const ref = React.useRef();
  const isVisible = useOnScreen(ref);
  const [showProgress, setShowProgress] = React.useState(false);

  React.useEffect(() => {
    if (isVisible) setShowProgress(true);
  }, [isVisible]);

  return (
    <div ref={ref}>
      {showProgress && (
        <Grid
          container
          justifyContent={"space-between"}
          className="pt-20 counter"
        >
          <ProgressCounter maxProgress={60} />
          <ProgressCounter maxProgress={75} />
          <ProgressCounter maxProgress={80} />
        </Grid>
      )}
    </div>
  );
}
