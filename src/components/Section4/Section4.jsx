import { Container, Grid } from "@mui/material";
import React from "react";
import Counter from "./Counter";

const Section4 = () => {
 return (
  <>
   <section className="section-4">
    <Container>
     <Grid container>
      <Grid item lg={12}>
       <h1>Premium Customers Report Better Results</h1>
       <p>We routinely survey Grammarly users and have found that:</p>
       <Counter />
      </Grid>
     </Grid>
    </Container>
   </section>
  </>
 );
};

export default Section4;
