import { Button, Container, Grid } from "@mui/material";
import React from "react";
import TabBanner from "./TabBanner";

const Section1 = () => {
 return (
  <>
   <section className="section-1">
    <Container>
     <Grid container className="justify-center">
      <Grid item lg={10} xs={12}>
       <div className="heading-text text-center">
        <h1>Aim High With Brilliant Writing</h1>
        <Button className="up-btn">Upgrade to Grammarly Premium</Button>
       </div>
       <TabBanner />
      </Grid>
     </Grid>
    </Container>
   </section>
  </>
 );
};

export default Section1;
