import * as React from "react";

import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";
import GrammarlySuggestion from "../../assets/images/Banner Tabs/GrammarlySuggestion";
import Suggestion2 from "../../assets/images/Banner Tabs/Suggestion2";
import Suggestion3 from "../../assets/images/Banner Tabs/Suggestion3";
import Writing from "../../assets/images/Banner Tabs/WritingSample.jpg";
import Writing2 from "../../assets/images/Banner Tabs/writing_sample2.jpg";
import Writing3 from "../../assets/images/Banner Tabs/writing_sample3.jpg";

export default function TabBanner() {
 return (
  <Tabs className="flex justify-between tab-banner ">
   <div className="tab-p flex-grow-1">
    <TabPanel>
     <div className="card-upper">
      <img src={Writing} alt="" className="img-fluid" />
     </div>
     <div className="lower-card">
      <GrammarlySuggestion />
     </div>
    </TabPanel>
    <TabPanel className={"flex-grow-1"}>
     <div className="card-upper">
      <img src={Writing2} alt="" className="img-fluid" />
     </div>
     <div className="lower-card">
      <Suggestion2 />
     </div>
    </TabPanel>
    <TabPanel className={"flex-grow-1"}>
     <div className="card-upper">
      <img src={Writing3} alt="" className="img-fluid" />
     </div>
     <div className="lower-card">
      <Suggestion3 />
     </div>
    </TabPanel>
   </div>

   <TabList className={"flex flex-col flex-grow-0"}>
    <Tab>
     <h2>Clear, confident communication</h2>
     <p>Take the guesswork out of great writing.</p>
    </Tab>
    <Tab>
     <h2>Support you can rely on</h2>
     <p>Write with a second pair of eyes that never gets tired.</p>
    </Tab>
    <Tab>
     <h2>Strike the right tone</h2>
     <p>Make the best impression, every time.</p>
    </Tab>
   </TabList>
  </Tabs>
 );
}
